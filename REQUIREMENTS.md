# SRE Code Test

### Problem

Write a simple command line interface (feel free to use any programming language
you're comfortable with) that accepts two different arguments:

- A path to a file. This file is a plaintext file that contains a number per line.
- An integer number `N` (with a max value of `30000000`)

With that in mind, your program will output the `N` largest numbers from the file,
sorted from highest to lowest (numbers can be repeated).

#### Requirements

Your program will have to comply with the following requirements:

- Must build and execute in an Unix operating system
- Exit with custom errors when using wrong arguments, for example:
  - Filename does not exist: `ERROR: input file does not exist.`
  - Filename exists but it is not readable: `ERROR: input file is not readable.`
  - The integer `N` argument is less or equal to 0: `ERROR: Number of top results must be bigger than 0.`
  - The integer `N` argument is over the upper limit: `ERROR: Maximum number of top results must be less or equal than 30000000.`
- The program will ignore and warn about incorrect lines, for example:
  - Such as multiple numbers on the same line `12345 890`
  - Or non numeric chars
    - Like `12345.01`
    - Or `1234hello`
  - Print warnings in the following format: `WARN: Invalid line <line>`
- The program will have to be as resource efficient as possible
- The program must be able to handle relatively small files (thousands of lines, sizing in KBs)
  and pretty big files (tens of millions of lines, sizing in GBs)
- Avoid using third party libraries and stick with the standard ones
  
### Tasks

- Provide the source code and instructions on how to build your program on this repository
- Answer the following questions (another text file on this repository will suffice):
  - What would be the `time` and `space` complexity of your program? Why?
  - Do you think there is still room for improvement in your solution? If so, please elaborate.
