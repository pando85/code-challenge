# sre-code-test-pando85

This program is wroten in [Rust](https://www.rust-lang.org/).

Answers could be found [here](ANALYSIS.md)

## Requirements

- [Rust tools](https://doc.rust-lang.org/book/ch01-01-installation.html)


## Build

```bash
cargo build --release
```

Binary can be found in `target/release/sre-code-test-pando85`.

## Run tests

```bash
cargo test
```

