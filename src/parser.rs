
pub fn parse_args(args: Vec<String>) -> Option<(String, usize)> {
    match args.len() {
        3 => {
            let filename: String = args[1].clone();
            let num = &args[2];
            let number: i64 = match num.parse() {
                Ok(n) => n,
                Err(_) => {
                    eprintln!("ERROR: second argument is not an integer.");
                    help();
                    return None;
                },
            };
            let n: usize = match check_number(number) {
                Some(n) => n,
                None => {
                    return None;
                },
            };
            return Some((filename, n));

        },
        _ => {
            help();
        }
    }
    return None;
}

fn help() {
    println!("usage:
sre-code-test-pando85 <filename> <integer>");
}

fn check_number(n: i64) -> Option<usize> {
    if n <= 0 {
        eprintln!("ERROR: Number of top results must be bigger than 0.");
        return None;
    };
    if n > 30000000 {
        eprintln!("ERROR: Maximum number of top results must be less or equal than 30000000.");
        return None;
    };

    return Some(n as usize);
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_args_ok() {
        let args: Vec<String> = vec![
            "sre-code-test-pando85".to_string(),
            "foo.txt".to_string(),
            "3000".to_string()];
        let parsed_args = parse_args(args).unwrap();
        assert_eq!(parsed_args.0, "foo.txt".to_string());
        assert_eq!(parsed_args.1, 3000);
    }

    #[test]
    fn test_parse_args_no_args() {
        let args: Vec<String> = vec!["sre-code-test-pando85".to_string()];
        assert_eq!(parse_args(args), None);
    }

    #[test]
    fn test_parse_args_no_filename() {
        let args: Vec<String> = vec![
            "sre-code-test-pando85".to_string(),
            "3000".to_string()];
        assert_eq!(parse_args(args), None);
    }

    #[test]
    fn test_parse_args_no_number() {
        let args: Vec<String> = vec![
            "sre-code-test-pando85".to_string(),
            "foo.txt".to_string()];
        assert_eq!(parse_args(args), None);
    }

    #[test]
    fn test_parse_args_not_integer() {
        let args: Vec<String> = vec![
            "sre-code-test-pando85".to_string(),
            "foo.txt".to_string(),
            "boo".to_string()];
        assert_eq!(parse_args(args), None);
    }

    #[test]
    fn test_parse_args_number_over_limits() {
        let args: Vec<String> = vec![
            "sre-code-test-pando85".to_string(),
            "foo.txt".to_string(),
            "9000000000000".to_string()];
        assert_eq!(parse_args(args), None);
    }

    #[test]
    fn test_parse_args_number_below_limits() {
        let args: Vec<String> = vec![
            "sre-code-test-pando85".to_string(),
            "foo.txt".to_string(),
            "-9000000000000".to_string()];
        assert_eq!(parse_args(args), None);
    }

    #[test]
    fn test_parse_args_number_equal_zero() {
        let args: Vec<String> = vec![
            "sre-code-test-pando85".to_string(),
            "foo.txt".to_string(),
            "0".to_string()];
        assert_eq!(parse_args(args), None);
    }


}
