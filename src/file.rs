use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use crate::sort;


pub fn process(filename: String, result: &mut Vec<i32>, max_results: usize) -> Option<&mut Vec<i32>> {

    if ! check_exists(&filename) {
        return None;
    }

    if let Ok(lines) = read_lines(&filename) {
        for line in lines {
            if let Ok(s) = line {
                if let Some(number) = get_line_number(&s) {
                    sort::insert_value(result, number, max_results);
                }
                else {
                    eprintln!("WARN: Invalid line {}", s);
                };
            }
            else {
                eprintln!("WARN: Invalid line");
            };
        };
    };

    return Some(result);
}

fn check_exists(filename: &String) -> bool {
    if ! Path::new(filename).exists() {
        eprintln!("ERROR: input file does not exist.");
        return false;
    }
    return true;
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = match File::open(&filename) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("ERROR: input file is not readable.");
            return Err(e);
        },
    };
    Ok(io::BufReader::new(file).lines())
}

fn get_line_number(line: &String) -> Option<i32> {
    let n: i32 = match line.parse() {
        Ok(n) => n,
        Err(_) => return None,
    };
    return Some(n);
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check_line_ok() {
        let line: String = "100000".to_string();
        assert_eq!(get_line_number(&line).unwrap(), 100000 as i32);
    }

    #[test]
    fn test_check_line_multiple_numbers() {
        let line: String = "10000 2414".to_string();
        assert_eq!(get_line_number(&line), None);
    }

    #[test]
    fn test_check_line_float() {
        let line: String = "12345.01".to_string();
        assert_eq!(get_line_number(&line), None);
    }

    #[test]
    fn test_check_line_text() {
        let line: String = "1234hello".to_string();
        assert_eq!(get_line_number(&line), None);
    }

}
