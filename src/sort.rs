pub fn insert_value(vector: &mut Vec<i32>, new_value: i32, max_results: usize) -> &mut Vec<i32> {
    if vector.len() < max_results {
        vector.push(new_value);
        return sort_last_value(vector);
    }
    else if &new_value > vector.last().unwrap() {
        let n = vector.len();
        vector[n - 1] = new_value;
        return sort_last_value(vector);
    }

    return vector;
}

/// Returns a reverse sorted vector
/// # Arguments
/// * `vector` - A reverse sorted vector except last value
/// # Example
/// ```
/// let x = &mut vec![3, 2, 1, 4];
/// let result = sort_last_value(x);
/// assert_eq!(result, &mut vec![4, 3, 2, 1]);
/// ```
pub fn sort_last_value(vector: &mut Vec<i32>) -> &mut Vec<i32> {
    for i in (0..vector.len() - 1).rev() {
        if vector[i + 1] > vector[i] {
            vector.swap(i, i + 1);
        } else {
            break;
        }
    }
    return vector;
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_insert_value_new_value() {
        let x = &mut vec![3, 2, 1];
        let new_value = 4;
        let max_results = 40;
        let expected_result : &mut Vec<i32> = &mut vec![4, 3, 2, 1];
        assert_eq!(insert_value(x, new_value, max_results), expected_result);
    }

    #[test]
    fn test_insert_value_new_value_full() {
        let x = &mut vec![3, 2, 1];
        let new_value = 4;
        let max_results = 3;
        let expected_result : &mut Vec<i32> = &mut vec![4, 3, 2];
        assert_eq!(insert_value(x, new_value, max_results), expected_result);
    }

    #[test]
    fn test_insert_value_new_value_low() {
        let x = &mut vec![3, 2, 1];
        let new_value = 0;
        let max_results = 3;
        let expected_result : &mut Vec<i32> = &mut vec![3, 2, 1];
        assert_eq!(insert_value(x, new_value, max_results), expected_result);
    }

    #[test]
    fn test_sort_last_value_biggest_value() {
        let x = &mut vec![3, 2, 1, 4];
        let expected_result : &mut Vec<i32> = &mut vec![4, 3, 2, 1];
        assert_eq!(sort_last_value(x), expected_result);
    }

    #[test]
    fn test_sort_last_value_negative_value() {
        let x : &mut Vec<i32> = &mut vec![3, 2, -10, -3];
        let result : &mut Vec<i32> = &mut vec![3, 2, -3, -10];
        assert_eq!(sort_last_value(x), result);
    }

    #[test]
    fn test_sort_last_value_incorrect_value() {
        let x : &mut Vec<i32> = &mut vec![3, 2, 1, 0];
        let result : &mut Vec<i32> = &mut vec![3, 2, 1, 0];
        assert_eq!(sort_last_value(x), result);
    }
}

