
use std::env;
use std::process;

mod parser;
mod file;
mod sort;


fn main() {
    let args: Vec<String> = env::args().collect();
    let parsed_args = match parser::parse_args(args) {
        Some(x) => x,
        None => {
            process::exit(1);
        },
    };

    let filename = parsed_args.0;
    let max_results = parsed_args.1;

    let result : &mut Vec<i32> = &mut Vec::new();
    match file::process(filename, result, max_results) {
        Some(x) => x,
        None => {
            process::exit(2);
        },
    };
    for n in result.iter() {
        println!("{}", n);
    };
}
