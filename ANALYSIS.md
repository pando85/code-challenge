
# What would be the `time` and `space` complexity of your program? Why?

## Time complexity


`M`: Valid file lines

`N`: Result len


Every sorting operation in worst case takes N iterations over the result and
it has to sort M numbers.

Time complexity is `O(N*M)`.

## Space complexity

Space complexity is `O(N)` because we need a vector of len `N` to store all result values.

# Do you think there is still room for improvement in your solution? If so, please elaborate.

Efficiency of this solution could be hardly increased.

But if we want to reduce time of execution we could parallelize reading and sorting using more CPUs
and memory. This solutions is using only one thread to execute the algorithm, we could read with
one thread and sort with other.
It depends what problem need to be solved.

